import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Fragment,useEffect ,useState} from 'react';
import { Navigate , useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Register(){
        //Create 3 new stats where we will store the value from input of the email
        
        const [firstname , setFirstName] = useState('');
        const [lastname , setLastName] = useState('');
        const [email , setEmail] = useState('');
        const [password , setPassword] = useState('');
        const [mobilenumber , setMobileNumber] = useState('');
        const [user , setUser] = useState('');
    
        

        const navigate = useNavigate();

        //create another State for the button

        const [isActive , setIsActive] = useState(false)

        // useEffect(()=>{
        //     console.log(email)
        //     console.log(password)
        //     console.log(confirmPassword)
        // },[email , password , confirmPassword])

        useEffect(()=>{
            if(email !== "" && password !== "" && mobilenumber !== "" && firstname !== "" && lastname !== ""){
                setIsActive(true)
            }else{
                setIsActive(false)
            }

        },[email,password, mobilenumber ,firstname ,lastname])

        function register(event){
            event.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
                method: "POST",
                body: JSON.stringify({
                  firstName: firstname,
                  lastName: lastname,
                  email: email,
                  password: password,
                  mobileNumber: mobilenumber
                }),
                headers:{
                    'Content-Type' : 'application/json'
                }
              })
              .then(result => result.json())
              .then(data => {
                console.log(data)
                if(data){
                  Swal.fire({
                    title: "Successfully enrolled!",
                    icon: "success",
                    text: "Please wait for further final schedule of the course"
                  })
                  navigate("/login")
                } else {
                  Swal.fire({
                    title: "Enrollment unsuccessful",
                    icon: "error",
                    text: "Please try again"
                  })
                }
              })


            
            
            
            
            // localStorage.setItem("email", email);
            // setUser(localStorage.getItem("email"))

            // alert("You are Registered on our Website!")
            // setEmail('');
            // setPassword('');
            // setConfirmPassword('')

            // navigate("/")

        }
       

    return (
        user?
            <Navigate to = "*"/>
        :
        <Fragment>
            <h1 className='text-center mt-5'>Register</h1>
            <Form className='mt-5' onSubmit={event => register(event)}>
             <Form.Group className="mb-3" controlId="formBasicFirstName">
                <Form.Label>First name</Form.Label>
                <Form.Control 
                        type="text" 
                        placeholder="Enter First Name"
                        value = {firstname}
                        onChange ={event => setFirstName(event.target.value)} 
                        required
                        />
               
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicLastName">
                <Form.Label>Last name</Form.Label>
                <Form.Control 
                        type="text" 
                        placeholder="Enter Last Name"
                        value = {lastname}
                        onChange ={event => setLastName(event.target.value)} 
                        required
                        />
              
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange ={event => setEmail(event.target.value)} 
                        required
                        />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value = {password}
                        onChange ={event => setPassword(event.target.value)}
                        required
                        />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                        type="text" 
                        placeholder="+(69)"
                        value = {mobilenumber}
                        onChange ={event => setMobileNumber(event.target.value)}
                        required />
            </Form.Group>
            {
                isActive?
                <Button variant="primary" type="submit">
                Submit
                 </Button>
                 :
                 <Button variant="danger" type="submit" disabled>
                Submit
                 </Button>
            }

            
            </Form>
        </Fragment>
    )
}